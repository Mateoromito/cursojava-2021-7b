package modulo3;

import java.util.Scanner;

public class punto11 {

public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		char letra;
		System.out.print("Ingrese una letra ----> ");
		letra = scan.next().charAt(0);
		
		if(letra=='a'||letra=='e'||letra=='i'||letra=='o'||letra=='u') {
			System.out.println("la letra "+letra+" es vocal");
		}
		else {
			System.out.println("la letra "+letra+" es consonante");
		}
		scan.close();

	}

}


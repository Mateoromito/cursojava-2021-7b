package modulo3;

import java.util.Scanner;

public class punto9 {

public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		String jugador1, jugador2;
		
		System.out.println("Piedra (0), Papel (1) o Tijeras (2) (usando and)");
		System.out.print("Jugador 1: ");
		jugador1 = scan.nextLine();
		System.out.print("Jugador 2: ");
		jugador2 = scan.nextLine();
		//re easy usar and porque no lo aprendi antes
		if(jugador1.equals("0") && jugador2.equals("0")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("1") && jugador2.equals("1")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("2") && jugador2.equals("2")) {
			System.out.println("Empate");
		}
		else if(jugador1.equals("0") && jugador2.equals("1")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("0") && jugador2.equals("2")) {
			System.out.println("Jugador 1 gana");
		}
		else if(jugador1.equals("1") && jugador2.equals("0")) {
			System.out.println("Jugador 1 gana");
		}
		else if(jugador1.equals("1") && jugador2.equals("2")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("2") && jugador2.equals("0")) {
			System.out.println("Jugador 2 gana");
		}
		else if(jugador1.equals("2") && jugador2.equals("1")) {
			System.out.println("Jugador 1 gana");
		}
		scan.close();

	}

}


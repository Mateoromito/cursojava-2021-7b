package modulo3;

import java.util.Scanner;

public class punto12 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int num;
		System.out.println("Indicador de rango de docenas");
		System.out.print("Ingrese su numero: ");
		num = scan.nextInt();
		
		if(num>=1 && num<=12) {
			System.out.println(num + " esta dentro de la primer docena");
		}
		else if(num>=13 && num<=24) {
			System.out.println(num + " esta dentro de la segunda docena");
		}
		else if(num>=25 && num<=36) {
			System.out.println(num + "\" esta dentro de la tercer docena");
		}
		else if(num<1 || num>36) {
			System.out.println(num + " esta fuera rango");
		}
		scan.close();

	}

}


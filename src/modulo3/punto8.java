package modulo3;

import java.util.Scanner;

public class punto8 {

	public static void main(String[] args) {


		Scanner scan = new Scanner(System.in);
		
		String player1;
		String player2;
		
		System.out.println("Piedra (0), Papel (1) o Tijeras (2)");
		System.out.print("Jugador 1 ----> ");
		player1 = scan.nextLine();
		System.out.print("Jugador 2 ----> ");
		player2 = scan.nextLine();
		
		if(player1.equals("0")) {
			if(player2.equals("1")) {
				System.out.println("player 2 gana");
			}
			else if(player2.equals("2")) {
				System.out.println("player 1 gana");
			}
			else {
				System.out.println("Empate");
			}
		}
		else if(player1.equals("2")) {
			if(player2.equals("0")) {
				System.out.println("player 2 gana");
			}
			else if(player2.equals("1")) {
				System.out.println("player 1 gana");
			}
			else {
				System.out.println("Empate");
			}
		}
		else if(player1.equals("1")) {
			if(player2.equals("0")) {
				System.out.println("player 1 gana");
			}
			else if(player2.equals("2")) {
				System.out.println("player 2 gana");
			}
			else {
				System.out.println("Empate");
			}
		
		}
		scan.close();

	}

}


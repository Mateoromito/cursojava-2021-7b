package modulo3;

import java.util.Scanner;

public class punto15 {

public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char categoria;
		
		System.out.print("Ingrese la categoria del auto A, B o C: ");
		categoria = scan.next().charAt(0);

		switch(categoria) {
		case 'c': case 'C':
			System.out.println("La categora "+categoria+" cuenta con : ");
			System.out.println("4 ruedas, Un motor, Sistema de cerradura centralizada, Sistema de aire acondicionado, Sistema de Airbag");
			break;
		case 'a': case 'A':
			System.out.println("La categora "+categoria+" cuenta con: ");
			System.out.println("4 ruedas, Un motor");
			break;
		case 'b': case 'B':
			System.out.println("La categora "+categoria+" cuenta con:");
			System.out.println("4 ruedas, Un motor, Sistema de cerradura centralizada, Sistema de aire acondicionado");
			break;
		default:
			System.out.println("Se pidio A, B, o C champion no pongas otra letra");
			break;
		}
		
		scan.close();

	}

}
